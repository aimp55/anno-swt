#!/bin/sh

JAVA_BIN=java
BASEDIR=`dirname $0`
echo "BASEDIR: ${BASEDIR}"
CLASSPATH=`find $BASEDIR/lib -type f -iname *jar | xargs | tr ' ' ':'`
echo "Classpath: ${CLASSPATH}"

exec $JAVA_BIN -cp "$CLASSPATH" org.genesys2.anno.gui.AppWindow
