package org.genesys2.anno.converter;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class RowConverterTest {
	private ObjectMapper mapper = new ObjectMapper();

	@Test(expected = JsonGenerationException.class)
	public void testEnsureParentNodeFail() throws JsonGenerationException {
		ObjectNode rowNode = mapper.createObjectNode();
		RowConverter.ensureNode(rowNode, "");
	}

	@Test
	public void testEnsureParentNode2() throws JsonGenerationException {
		ObjectNode rowNode = mapper.createObjectNode();
		JsonNode parentNode;
		parentNode = RowConverter.ensureNode(rowNode, "geo");
		assertTrue("Expecting parentNode==rowNode", parentNode == rowNode);
	}

	@Test
	public void testEnsureParentNodeColl() throws JsonGenerationException {
		ObjectNode rowNode = mapper.createObjectNode();
		JsonNode parentNode;
		parentNode = RowConverter.ensureNode(rowNode, "coll.date");
		assertTrue("Expecting parentNode!=rowNode", parentNode != rowNode);
		assertTrue("Cannot find new parent node", rowNode.get("coll") != null);
		assertTrue("Parent node is not an object node", rowNode.get("coll").isObject());
		assertTrue("Parent node returned is not found", rowNode.get("coll") == parentNode);
	}

	@Test
	public void testEnsureParentNodeDeep() throws JsonGenerationException {
		ObjectNode rowNode = mapper.createObjectNode();
		JsonNode parentNode;
		String nodeName = "coll.foo.bar.xxx.yyy";
		parentNode = RowConverter.ensureNode(rowNode, nodeName);
		assertTrue("Expecting parentNode!=rowNode", parentNode != rowNode);
		assertTrue("Cannot find new parent node", rowNode.get("coll") != null);

		String[] namePath = nodeName.split("\\.");
		JsonNode currentNode = rowNode;
		for (String p : namePath) {
			currentNode = currentNode.path(p);
		}
	}
}
