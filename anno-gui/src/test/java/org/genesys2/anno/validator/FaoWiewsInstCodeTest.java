/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.genesys2.anno.model.ColumnValidator;
import org.junit.Test;

/**
 * Test validation of FAO WIEWS codes
 * 
 * @author matijaobreza
 * 
 */
public class FaoWiewsInstCodeTest {

	@Test
	public void testBlanks() {
		ColumnValidator validator = new FaoWiewsInstCodeValidator();

		// Null is valid
		assertTrue(validator.isValid(null, null));
		// Blanks are valid
		assertTrue(validator.isValid(null, "   "));
	}

	@Test
	public void testTrim() {
		ColumnValidator validator = new FaoWiewsInstCodeValidator();

		// value must be trimmed
		assertFalse(validator.isValid(null, "   NGA03911   "));
		// value must be trimmed
		assertTrue(validator.isValid(null, "NGA03911"));
		// value must be trimmed
		assertFalse(validator.isValid(null, "NGA03911  "));
		// value must be trimmed
		assertFalse(validator.isValid(null, "NGA03911\t"));
	}

	@Test
	public void testFormat() {
		ColumnValidator validator = new FaoWiewsInstCodeValidator();

		// Format matches
		assertFalse(validator.isValid(null, "123"));
		assertFalse(validator.isValid(null, "USA"));
		assertFalse(validator.isValid(null, "1234USA"));
		assertFalse(validator.isValid(null, "USA1"));
		assertFalse(validator.isValid(null, "AVQ12"));
		assertTrue(validator.isValid(null, "NGA039"));
		assertFalse(validator.isValid(null, "NGA 039"));
		assertTrue(validator.isValid(null, "NGA0391"));
		assertTrue(validator.isValid(null, "NGA03911"));
	}

}
