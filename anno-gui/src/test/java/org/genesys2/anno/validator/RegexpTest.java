/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.validator;

import static org.junit.Assert.assertTrue;

import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnValidator;
import org.junit.Test;

/**
 * Test validation of FAO WIEWS codes
 * 
 * @author matijaobreza
 * 
 */
public class RegexpTest {

	@Test
	public void testBlankValue() {
		ColumnValidator validator = new RegexpValidator();

		assertTrue(validator.isValid(null, null));
		assertTrue(validator.isValid(null, "     "));
		assertTrue(validator.isValid(null, "   \r\t\n\t "));
		assertTrue(validator.isValid(new Column(), "   \r\t\n\t "));
		Column column = new Column();
		column.setPattern("\\w+");
		assertTrue(validator.isValid(column, "   \r\t\n\t "));
	}

	@Test
	public void testNullColumn() {
		ColumnValidator validator = new RegexpValidator();

		assertTrue(validator.isValid(null, "askjdha  asdkljhas laksj1  12312"));
	}

	@Test
	public void testBlankPattern() {
		ColumnValidator validator = new RegexpValidator();

		Column column = new Column();
		column.setPattern(null);
		assertTrue(validator.isValid(column, "askjdha  asdkljhas laksj1  12312"));

		column.setPattern("   ");
		assertTrue(validator.isValid(column, "askjdha  asdkljhas laksj1  12312"));
	}
}
