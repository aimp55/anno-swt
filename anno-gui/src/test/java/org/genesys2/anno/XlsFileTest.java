/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.genesys2.anno.reader.StopParsingException;
import org.genesys2.anno.reader.XSSFReadWrite;
import org.genesys2.anno.reader.XSSFRowReader;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class XlsFileTest {
	@Test
	public void test1() {
		System.err.println("Test 1");
		File file = new File("/Users/matijaobreza/Downloads", "AVRDC.xlsx");
		try {
			XSSFReadWrite.dumpFile(file.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test2() {
		System.err.println("Test 2");
		File file = new File("/Users/matijaobreza/Downloads", "AVRDC.xlsx");
		try {
			XSSFReadWrite.processAllSheets(file.getAbsolutePath());
		} catch (StopParsingException e) {
			// NOP
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void test3() {
		System.err.println("Test 3");
		File file = new File("/Users/matijaobreza/Downloads", "AVRDC.xlsx");
		try {
			XSSFRowReader.processAllSheets(file.getAbsolutePath());
		} catch (StopParsingException e) {
			// NOP
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testNameToColumn() {
		assertTrue(0 == XSSFRowReader.nameToColumn("A"));
		assertTrue(1 == XSSFRowReader.nameToColumn("B"));
		assertTrue(2 == XSSFRowReader.nameToColumn("C"));
		assertTrue(0 == XSSFRowReader.nameToColumn("A1"));
		assertTrue(0 == XSSFRowReader.nameToColumn("A13"));
		assertTrue(26 == XSSFRowReader.nameToColumn("AA13"));
	}

	@Test
	public void test3Rows() {
		System.err.println("Test 3");
		File file = new File("/Users/matijaobreza/Downloads", "AVRDC.xlsx");
		try {
			List<Object[]> rows = XSSFRowReader.getRows(file.getAbsolutePath(), 10);
			for (Object[] row : rows) {
				System.out.println("len=" + row.length);
				System.out.println(ArrayUtils.toString(row));
			}
		} catch (StopParsingException e) {
			// NOP
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
