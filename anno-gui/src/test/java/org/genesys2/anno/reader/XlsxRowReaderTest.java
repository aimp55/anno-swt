package org.genesys2.anno.reader;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.genesys2.anno.parser.RowReader;
import org.genesys2.anno.parser.XlsxRowReader;
import org.junit.Test;

public class XlsxRowReaderTest {
	private static final Logger _log = Logger.getLogger(XlsxRowReaderTest.class);

	@Test(expected = IllegalArgumentException.class)
	public void testNofile() throws IOException {
		RowReader rowReader = new XlsxRowReader(null, null);
		rowReader.close();
	}

	private File getTestFile(String resourcePath) {
		URL x = getClass().getResource(resourcePath);
		Path path;
		try {
			path = Paths.get(x.toURI());
			return path.toFile();
		} catch (NullPointerException e) {
			_log.warn("Resource URI not found for " + resourcePath);
			return null;
		} catch (URISyntaxException e) {
			return null;
		}
	}

	@Test
	public void fileExists() {
		File sourceFile = getTestFile("/xlsx/onesheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		assertTrue(sourceFile.exists());
		assertTrue(sourceFile.canRead());
	}

	@Test
	public void testNoSheet() throws IOException {
		File sourceFile = getTestFile("/xlsx/onesheet.xlsx");
		try {
			RowReader rowReader = new XlsxRowReader(sourceFile, null);
			rowReader.close();
		} catch (IllegalArgumentException e) {
			// ok
		}
	}

	@Test
	public void testFirstSheet() throws IOException {
		File sourceFile = getTestFile("/xlsx/threesheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "onesheet");
		rowReader.close();
	}

	@Test
	public void testSecondSheet() throws IOException {
		File sourceFile = getTestFile("/xlsx/threesheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "twosheet");
		rowReader.close();
	}

	@Test
	public void testFirstOfTwoSheets() throws IOException {
		File sourceFile = getTestFile("/xlsx/threesheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "onesheet");
		rowReader.close();
	}

	@Test
	public void testReaderNoRead() throws IOException {
		File sourceFile = getTestFile("/xlsx/twosheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "onesheet");
		rowReader.close();
	}

	@Test
	public void testRead1Row() throws IOException {
		File sourceFile = getTestFile("/xlsx/twosheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "onesheet");
		List<Object[]> rows = rowReader.readRows(1);
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 1 row", rows.size() == 1);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}
		rowReader.close();
	}

	@Test
	public void testRead1By1Row() throws IOException {
		File sourceFile = getTestFile("/xlsx/twosheet.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "onesheet");
		List<Object[]> rows = null;
		rows = rowReader.readRows(1);
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 1 row", rows.size() == 1);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}

		rows = rowReader.readRows(1);
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 1 row", rows.size() == 1);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}
		rowReader.close();
	}

	@Test
	public void testReadMoreThanAvailable() throws IOException {
		File sourceFile = getTestFile("/xlsx/formulas.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		// "littledata" has 10 rows
		RowReader rowReader = new XlsxRowReader(sourceFile, "littledata");
		List<Object[]> rows = rowReader.readRows(100);
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 10 rows", rows.size() == 10);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}
		rowReader.close();
	}

	@Test
	public void testReadLessThanAvailable() throws IOException {
		File sourceFile = getTestFile("/xlsx/formulas.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		// "bigdata" has > 300 rows
		RowReader rowReader = new XlsxRowReader(sourceFile, "bigdata");
		_log.info("Reader ready");
		List<Object[]> rows = rowReader.readRows(10);
		_log.info("Got results");
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 10 rows", rows.size() == 10);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}
		rowReader.close();
	}

	@Test
	public void testReadAllData() throws IOException {
		File sourceFile = getTestFile("/xlsx/formulas.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		// "bigdata" has > 300 rows
		RowReader rowReader = new XlsxRowReader(sourceFile, "bigdata");
		List<Object[]> rows = null;

		do {
			rows = rowReader.readRows(10);
			assertTrue("Expecting non-null result", rows != null);
			for (Object[] row : rows) {
				_log.debug(Arrays.toString(row));
			}
		} while (rows != null && rows.size() > 0);

		rowReader.close();
	}

	@Test
	public void testReadFormulas() throws IOException {
		File sourceFile = getTestFile("/xlsx/formulas.xlsx");
		assertTrue("Test resource not found", sourceFile != null);
		RowReader rowReader = new XlsxRowReader(sourceFile, "formulas");
		List<Object[]> rows = rowReader.readRows(10);
		assertTrue("Expecting non-null result", rows != null);
		assertTrue("Expecting more than 0 rows", rows.size() > 0);
		assertTrue("Expected 1 row", rows.size() == 6);
		for (Object[] row : rows) {
			_log.debug(Arrays.toString(row));
		}
		rowReader.close();
	}
}
