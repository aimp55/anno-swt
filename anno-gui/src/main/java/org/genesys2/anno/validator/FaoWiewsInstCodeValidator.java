/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.validator;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnValidator;

/**
 * Format: ISO3 code + at least three digits
 * 
 * @author matijaobreza
 * 
 */
public class FaoWiewsInstCodeValidator implements ColumnValidator {

	@Override
	public boolean isValid(Column columnn, String value) {
		if (StringUtils.isBlank(value)) {
			return true;
		}
		// TODO FIXME Should check against actual FAO WIEWS institute codes!
		return Pattern.matches("^(\\w){3}(\\d){3}\\d*$", value);
	}

}
