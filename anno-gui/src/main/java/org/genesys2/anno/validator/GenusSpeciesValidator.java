/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.validator;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.RowValidator;
import org.genesys2.anno.predefined.RdfMCPD;

public class GenusSpeciesValidator implements RowValidator {

	@Override
	public boolean isValid(Column[] columns, String[] values) {
		if (columns == null || values == null) {
			return true;
		}

		boolean hasGenus = false, hasSpecies = false;
		String genusValue = null, speciesValue = null;

		// find genus in columns
		for (int i = 0; genusValue == null && i < columns.length; i++) {
			Column column = columns[i];
			if (column.getRdfTerm().equals(RdfMCPD.GENUS)) {
				hasGenus = true;
				genusValue = values.length > i ? values[i] : null;
			}
		}

		// find species in columns
		for (int i = 0; genusValue == null && i < columns.length; i++) {
			Column column = columns[i];
			if (column.getRdfTerm().equals(RdfMCPD.SPECIES)) {
				hasSpecies = true;
				speciesValue = values.length > i ? values[i] : null;
			}
		}

		if (hasGenus && hasSpecies && StringUtils.isNotBlank(genusValue) && StringUtils.isNotBlank(speciesValue)) {
			return true;
		}

		return false;
	}

}
