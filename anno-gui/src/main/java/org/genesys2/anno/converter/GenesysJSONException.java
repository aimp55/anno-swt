package org.genesys2.anno.converter;

public class GenesysJSONException extends Exception {

	public GenesysJSONException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
