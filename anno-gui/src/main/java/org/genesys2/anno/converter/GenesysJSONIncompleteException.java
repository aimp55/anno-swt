package org.genesys2.anno.converter;

public class GenesysJSONIncompleteException extends GenesysJSONException {

	public GenesysJSONIncompleteException(String message) {
		super(message);
	}

}
