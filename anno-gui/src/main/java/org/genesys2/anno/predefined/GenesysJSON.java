/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.predefined;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class GenesysJSON {
	public static class JsonField {
		private String fieldName;
		private String rdfTerm;
		private boolean allowMultiple = false;
		private Class<?> type = String.class;
		private boolean required;

		public JsonField(String fieldName, String rdfTerm) {
			this.fieldName = fieldName;
			this.rdfTerm = rdfTerm;
		}

		public JsonField setAllowMultiple(boolean allowMultiple) {
			this.allowMultiple = allowMultiple;
			return this;
		}

		public boolean hasAllowMultiple() {
			return this.allowMultiple;
		}

		public String getFieldName() {
			return fieldName;
		}

		public String getRdfTerm() {
			return rdfTerm;
		}

		public Class<?> getType() {
			return this.type;
		}

		public JsonField setType(Class<?> type) {
			this.type = type;
			return this;
		}

		public JsonField setRequired(boolean b) {
			this.required = b;
			return this;
		}

		public boolean isRequired() {
			return required;
		}
	}

	private final List<JsonField> jsonFields;
	private Collection<JsonField> requiredFields;

	public GenesysJSON() {
		List<JsonField> columns = new ArrayList<JsonField>();
		JsonField instCode = new JsonField(Api1Constants.Accession.INSTCODE, RdfMCPD.INSTCODE).setRequired(true);
		columns.add(instCode);

		JsonField acceNumb = new JsonField(Api1Constants.Accession.ACCENUMB, RdfMCPD.ACCENUMB).setRequired(true);
		columns.add(acceNumb);
		
		JsonField newAcceNumb = new JsonField(Api1Constants.Accession.ACCENUMB_NEW, RdfMCPD.NEWACCENUMB).setRequired(false);
		columns.add(newAcceNumb);

		JsonField genus = new JsonField(Api1Constants.Accession.GENUS, RdfMCPD.GENUS).setRequired(true);
		columns.add(genus);

		JsonField species = new JsonField(Api1Constants.Accession.SPECIES, RdfMCPD.SPECIES);
		columns.add(species);

		JsonField columnDef;
		columnDef = new JsonField(Api1Constants.Accession.SPAUTHOR, RdfMCPD.SPAUTHOR);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.SUBTAXA, RdfMCPD.SUBTAXA);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.SUBTAUTHOR, RdfMCPD.SUBTAUTHOR);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.ACCENAME, RdfMCPD.ACCENAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.ACQDATE, RdfMCPD.ACQDATE);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.ORIGCTY, RdfMCPD.ORIGCTY);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLNUMB, RdfMCPD.COLLNUMB);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLDATE, RdfMCPD.COLLDATE);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLCODE, RdfMCPD.COLLCODE).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLNAME, RdfMCPD.COLLNAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLSITE, RdfMCPD.COLLSITE);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLINSTADDRESS, RdfMCPD.COLLINSTADDRESS).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLMISSID, RdfMCPD.COLLMISSID);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.COLL +"." + Api1Constants.Collecting.COLLSRC, RdfMCPD.COLLSRC).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.LATITUDE, RdfMCPD.DECLATITUDE).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.LONGITUDE, RdfMCPD.DECLONGITUDE).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.COORDUNCERT, RdfMCPD.COORDUNCERT).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.COORDDATUM, RdfMCPD.COORDDATUM);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.GEOREFMETH, RdfMCPD.GEOREFMETH);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.GEO +"." + Api1Constants.Geo.ELEVATION, RdfMCPD.ELEVATION).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.BREDCODE, RdfMCPD.BREDCODE);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.SAMPSTAT, RdfMCPD.SAMPSTAT).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.ANCEST, RdfMCPD.ANCEST);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.DONORCODE, RdfMCPD.DONORCODE);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.DONORNUMB, RdfMCPD.DONORNUMB);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.DONORNAME, RdfMCPD.DONORNAME);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.OTHERNUMB, RdfMCPD.OTHERNUMB).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.DUPLSITE, RdfMCPD.DUPLSITE).setAllowMultiple(true);
		columns.add(columnDef);

		// columnDef = new JsonField("duplInstName",
		// RdfMCPD.DUPLINSTNAME).setAllowMultiple(true);
		// columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.STORAGE, RdfMCPD.STORAGE).setAllowMultiple(true).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.MLSSTAT, RdfMCPD.MLSSTAT).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.INTRUST, RdfMCPD.FAOINTRUST).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.AVAILABLE, RdfMCPD.AVAILABLE).setType(boolean.class);
		columns.add(columnDef);
		
		columnDef = new JsonField(Api1Constants.Accession.HISTORIC, RdfMCPD.HISTORIC).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField(Api1Constants.Accession.REMARKS, RdfMCPD.REMARKS).setAllowMultiple(true);
		columns.add(columnDef);

		this.jsonFields = Collections.unmodifiableList(columns);

		@SuppressWarnings("unchecked")
		Collection<JsonField> requiredFields = CollectionUtils.select(columns, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				JsonField jsonField = (JsonField) object;
				return jsonField.isRequired();
			}

		});
		this.requiredFields = Collections.unmodifiableCollection(requiredFields);
	}

	public final List<JsonField> getJsonFields() {
		return jsonFields;
	}

	public Collection<JsonField> getRequiredFields() {
		return requiredFields;
	}
}
