package org.genesys2.anno.gui;

import java.io.File;
import java.util.List;

public interface IDataSource {

	public File getFile();

	public void setFile(File file);

	public String getFileName();

	public List<IDataSourceSheet> getSheets();

	void setSheets(List<IDataSourceSheet> sheets);

    void removeSheet(IDataSourceSheet sourceSheet);
}
