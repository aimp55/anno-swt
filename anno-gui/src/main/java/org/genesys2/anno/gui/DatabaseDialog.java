package org.genesys2.anno.gui;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.model.JdbcDriver;
import org.genesys2.anno.model.JdbcDrivers;
import org.genesys2.anno.util.ConnectionUtils;
import org.genesys2.anno.util.DownloadUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import swing2swt.layout.BorderLayout;

public class DatabaseDialog extends Dialog {
	private static final Logger _log = Logger.getLogger(DatabaseDialog.class);
	private DataBindingContext m_bindingContext;

    @Value("${genesys.magic.workspace}")
    private String workspacePath;

	@Autowired
	private DatabaseSettings databaseSettings;

	@Autowired
	private DwcaBuilder builder;

    @Autowired
    private JdbcDrivers drivers;

    @Autowired
    private ExtraClassLoader extraClassLoader;

	protected Object result;
	protected Shell shell;
	private TreeViewer treeViewer;
	private Text txtName;
	private Text txtDbHost;
	private Text txtDbPort;
	private Text txtDbName;
	private Text txtDbUrl;
	private Text txtDbUserName;
	private Text txtDbPassword;
	private Combo comboType;

	/**
	 * Create the dialog.
	 *
	 * @param parent
	 * @param style
	 */
	public DatabaseDialog(Shell parent, TreeViewer treeViewer, int style) {
		super(parent, style);
		this.treeViewer = treeViewer;
		setText("Add Database");
	}

	/**
	 * Open the dialog.
	 *
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.SHELL_TRIM);
		shell.setSize(665, 445);
		shell.setText(getText());
		shell.setLayout(new BorderLayout(0, 0));

		ScrolledComposite scrolledComposite = new ScrolledComposite(shell, SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		Group grpDbConfiguration = new Group(composite, SWT.NONE);
		grpDbConfiguration.setLayout(new GridLayout(2, false));
		grpDbConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grpDbConfiguration.setText("Database configuration");
		/* Datasource type */
		Label lblType = new Label(grpDbConfiguration, SWT.NONE);
		lblType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblType.setText("Datasource type");

        comboType = new Combo(grpDbConfiguration, SWT.NONE);
        comboType.setItems(getDriversNames(drivers.getJdbcDrivers()));
        comboType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        comboType.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                List<JdbcDriver> jdbcDrivers=drivers.getJdbcDrivers();
                JdbcDriver driver=jdbcDrivers.get(jdbcDrivers.indexOf(new JdbcDriver(comboType.getText())));

                databaseSettings.setUrl(driver.getPredefinedConnectUrl());
                databaseSettings.setDriverClassName(driver.getDriverClassName());
                m_bindingContext = initDataBindings();
            }
        });
        /* Datasource name */
		Label lblName = new Label(grpDbConfiguration, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblName.setAlignment(SWT.RIGHT);
		lblName.setText("Datasource name");

		txtName = new Text(grpDbConfiguration, SWT.BORDER);
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		/* Connection URL */
		Label lblDbUrl = new Label(grpDbConfiguration, SWT.NONE);
		lblDbUrl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblDbUrl.setAlignment(SWT.RIGHT);
		lblDbUrl.setText("Connection URL");

		txtDbUrl = new Text(grpDbConfiguration, SWT.BORDER);
		txtDbUrl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		/* User */
		Label lblDbUserName = new Label(grpDbConfiguration, SWT.NONE);
		lblDbUserName.setAlignment(SWT.RIGHT);
		lblDbUserName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDbUserName.setText("User");

		txtDbUserName = new Text(grpDbConfiguration, SWT.BORDER);
		txtDbUserName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		/* Password */
		Label lblDbPassword = new Label(grpDbConfiguration, SWT.NONE);
		lblDbPassword.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDbPassword.setText("Password");
		lblDbPassword.setAlignment(SWT.RIGHT);

		txtDbPassword = new Text(grpDbConfiguration, SWT.BORDER);
		txtDbPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Composite compositeFooter = new Composite(composite, SWT.NONE);
		GridData gdFooter = new GridData(SWT.CENTER, SWT.TOP, true, false, 1, 1);
		compositeFooter.setLayoutData(gdFooter);
		compositeFooter.setLayout(new RowLayout(SWT.HORIZONTAL));

        Button btnDownloadFile = new Button(compositeFooter, SWT.NONE);
        btnDownloadFile.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
               List<JdbcDriver> jdbcDrivers=drivers.getJdbcDrivers();
               JdbcDriver driver=jdbcDrivers.get(jdbcDrivers.indexOf(new JdbcDriver(comboType.getText())));
               doDownload(driver);
            }
        });
        btnDownloadFile.setText("Download driver");

		Button btnConnect = new Button(compositeFooter, SWT.NONE);
		btnConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
                doConnect();
			}
		});
		btnConnect.setText("Connect");

		composite.pack();
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

	}

    private String[] getDriversNames(List<JdbcDriver> drivers) {
        String[] array = new String[drivers.size()];
        List<String> titles = new ArrayList<String>();
        for (JdbcDriver driver : drivers) {
            titles.add(driver.getTitle());
        }
        return titles.toArray(array);

    }

    private void doDownload(JdbcDriver driver) {
        String jdbcPath = workspacePath + "/jdbc";
        try {
            DownloadUtils.downloadFile(driver.getDownloadUrl(), jdbcPath);
            SwtUtil.showMessageBox(getParent(), "Success!", "Driver downloaded from\n" + driver.getDownloadUrl());
            extraClassLoader.addJars(new File(jdbcPath));
        } catch (IOException e) {
            _log.error(e.getMessage());
            SwtUtil.showMessageBox(getParent(), "JDBC Driver", e.getMessage());
        }
    }

	private void doConnect() {
		Connection connection = null;
		String message = "N/A";

		try {
			connection = ConnectionUtils.getConnection(databaseSettings.getDriverClassName(),this.txtDbUrl.getText(), this.txtDbUserName.getText(), this.txtDbPassword.getText());
		} catch (ClassNotFoundException e) {
			message = "Could not load JDBC driver " + e.getMessage();
		} catch (SQLException e) {
			message = e.getMessage();
		}

		MessageBox dialog = null;
		if (connection == null) {
			dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.CANCEL);

			dialog.setText("Connection");
			dialog.setMessage(message);
			dialog.open();

		} else {
			dialog = new MessageBox(shell, SWT.ICON_WORKING | SWT.OK | SWT.CANCEL);

			dialog.setText("Connection");
			dialog.setMessage("Connection successful!\nDo you want to add connection to workspace?");
			if (dialog.open() == SWT.OK) {
				doConnection(connection);
			}
		}
	}

	private void doConnection(Connection connection) {
		JdbcDataSource jdbcDataSource = new JdbcDataSource(txtName.getText());
		jdbcDataSource.setSettings(databaseSettings);
		builder.addDataSource(jdbcDataSource);
		treeViewer.refresh();
		this.shell.close();
		ConnectionUtils.close(connection);
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue ovTextName = WidgetProperties.text(SWT.Modify).observe(this.txtName);
		IObservableValue ovName = BeanProperties.value("dataSourceName").observe(databaseSettings);
		bindingContext.bindValue(ovTextName, ovName, null, null);
		//
		IObservableValue ovTextDbUrl = WidgetProperties.text(SWT.Modify).observe(txtDbUrl);
		IObservableValue ovDbUrl = BeanProperties.value("url").observe(databaseSettings);
		bindingContext.bindValue(ovTextDbUrl, ovDbUrl, null, null);
		//
		IObservableValue ovTextDbUser = WidgetProperties.text(SWT.Modify).observe(txtDbUserName);
		IObservableValue ovDbUser = BeanProperties.value("user").observe(databaseSettings);
		bindingContext.bindValue(ovTextDbUser, ovDbUser, null, null);
		//
		IObservableValue ovTextDbPassword = WidgetProperties.text(SWT.Modify).observe(txtDbPassword);
		IObservableValue ovDbPassword = BeanProperties.value("password").observe(databaseSettings);
		bindingContext.bindValue(ovTextDbPassword, ovDbPassword, null, null);
		//
		return bindingContext;
	}
}
