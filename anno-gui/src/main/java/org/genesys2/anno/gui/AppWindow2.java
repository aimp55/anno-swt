/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import swing2swt.layout.BorderLayout;

public class AppWindow2 {

	protected Shell shlGenesysMagic;
	private MenuItem mntmQuit;
	private Table table;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AppWindow2 window = new AppWindow2();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlGenesysMagic.open();
		shlGenesysMagic.layout();
		while (!shlGenesysMagic.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public static boolean isMac() {
		if (System.getProperty("os.name").equals("Mac OS X")) {
			return true;
		}
		return false;
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlGenesysMagic = new Shell();
		shlGenesysMagic.setMinimumSize(new Point(100, 22));
		shlGenesysMagic.setSize(554, 374);
		shlGenesysMagic.setText("Genesys Magic");
		shlGenesysMagic.setLayout(new BorderLayout(5, 5));

		Menu menu = new Menu(shlGenesysMagic, SWT.BAR);
		shlGenesysMagic.setMenuBar(menu);

		MenuItem mntmFile = new MenuItem(menu, SWT.CASCADE);
		mntmFile.setText("File");

		Menu menu_1 = new Menu(mntmFile);
		mntmFile.setMenu(menu_1);

		MenuItem mntmOpen = new MenuItem(menu_1, SWT.NONE);
		mntmOpen.setText("Open");

		new MenuItem(menu_1, SWT.SEPARATOR);

		mntmQuit = new MenuItem(menu_1, SWT.NONE);
		mntmQuit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				System.err.println("Exiting now.");
				shlGenesysMagic.dispose();
			}
		});
		mntmQuit.setText("Quit");

		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");

		Menu menu_2 = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_2);

		MenuItem mntmAbout = new MenuItem(menu_2, SWT.NONE);
		mntmAbout.setText("About");

		new MenuItem(menu_2, SWT.SEPARATOR);

		MenuItem mntmVersion = new MenuItem(menu_2, SWT.NONE);
		mntmVersion.setText("Version...");

		SashForm sashForm = new SashForm(shlGenesysMagic, SWT.NONE);
		sashForm.setLayoutData(BorderLayout.CENTER);

		table = new Table(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnColumn = new TableColumn(table, SWT.NONE);
		tblclmnColumn.setWidth(100);
		tblclmnColumn.setText("Column");

		TableColumn tblclmnRdfTerm = new TableColumn(table, SWT.NONE);
		tblclmnRdfTerm.setWidth(200);
		tblclmnRdfTerm.setText("Ontology Term");

		Tree tree = new Tree(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);

		TreeColumn trclmnTerms = new TreeColumn(tree, SWT.NONE);
		trclmnTerms.setWidth(200);
		trclmnTerms.setText("Terms");

		TreeColumn trclmnUrl = new TreeColumn(tree, SWT.NONE);
		trclmnUrl.setWidth(100);
		trclmnUrl.setText("URL");

		TreeItem trtmSomeTree = new TreeItem(tree, SWT.NONE);
		trtmSomeTree.setChecked(true);
		trtmSomeTree.setText("Some tree");
		trtmSomeTree.setExpanded(true);

		TreeItem trtmNewTreeitem_1 = new TreeItem(tree, SWT.NONE);
		trtmNewTreeitem_1.setText("Bugger");

		TreeItem trtmNewTreeitem = new TreeItem(tree, SWT.NONE);
		trtmNewTreeitem.setText(new String[] { "Alpha", "beta" });
		trtmNewTreeitem.setText("Another treee");
		sashForm.setWeights(new int[] { 310, 239 });

	}
}
