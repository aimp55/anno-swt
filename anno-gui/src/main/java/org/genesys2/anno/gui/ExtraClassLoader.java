package org.genesys2.anno.gui;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.log4j.Logger;

public class ExtraClassLoader extends URLClassLoader {
	private static final Logger _log = Logger.getLogger(AppWindow.class);

	public ExtraClassLoader(ClassLoader parentClassLoader) {
		super(new URL[] {}, parentClassLoader);
	}

	public void addJars(File baseDir) throws MalformedURLException {
		if (!baseDir.isDirectory()) {
			_log.warn("Not a directory: " + baseDir);
			return;
		}
		if (!baseDir.exists() || !baseDir.canRead()) {
			_log.warn("Cannot read directory: " + baseDir);
			return;
		}

		File[] jarFiles = baseDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".jar");
			}
		});

		for (File jarFile : jarFiles) {
			addJar(jarFile);
		}
	}

	private void addJar(File jarFile) throws MalformedURLException {
		URL fileUrl = jarFile.toURI().toURL();
		System.err.println("URL: " + fileUrl);
		URL url=new URL("jar:" + fileUrl.toString() + "!/");
		//new URL("jar:file://" + jarFile.getAbsolutePath() + "!/");
		_log.info("Adding JAR file " + fileUrl);
		addURL(fileUrl);
	}
}