package org.genesys2.anno.gui;

import org.genesys2.anno.model.Column;

import java.io.File;
import java.util.List;

public interface IDataSourceSheet {

	public String getQuery();

	public void setQuery(String query);

	public void setSheetName(String sheetName);

	public String getSheetName();

	void setHeadersIncluded(boolean headersIncluded);

	public boolean isHeadersIncluded();

	void setHeaderRowIndex(int headerRowIndex);

	public int getHeaderRowIndex();

	public void updateData(List<Object[]> rows);

	public List<Object[]> getSampleData();

	public List<Column> getColumns();

	public void setColumns(List<Column> columns);

	public boolean isCsv();

	public boolean isSqlQuery();

	public File getSourceFile();

	public void automap(ColumnDefs columnDefs);
}
