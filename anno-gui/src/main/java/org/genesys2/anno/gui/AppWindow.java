/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.*;
import org.eclipse.wb.swt.SWTResourceManager;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.model.Settings;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import swing2swt.layout.BorderLayout;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class AppWindow {
	private static final Logger _log = Logger.getLogger(AppWindow.class);

	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	protected DataSourceLoader dataSourceLoader;

	@Autowired
	private DwcaBuilder builder;

	@Autowired
	private ColumnDefs columnDefs;

	@Autowired
	private Settings settings;

	private ExecutorService threadPool = Executors.newFixedThreadPool(4);

	private static class ViewerLabelProvider extends LabelProvider {
		@Override
		public Image getImage(Object element) {
			_log.debug("getImage " + element);
			if (element instanceof IDataSource) {
				return archiveIcon;
			}
			if (element instanceof IDataSourceSheet) {
				return spreadsheetIcon;
			}
			return super.getImage(element);
		}

		@Override
		public String getText(Object element) {
			// _log.debug("getText " + element);
			if (element instanceof DataSourceFile) {
				return ((DataSourceFile) element).getFile().getName();
			}
			if (element instanceof DataSourceSheet) {
				return ((DataSourceSheet) element).getSheetName();
			}
			return super.getText(element);
		}
	}

	private static class TreeContentProvider implements ITreeContentProvider {
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			_log.debug("getElements " + inputElement + " " + inputElement.getClass());
			if (inputElement instanceof DwcaBuilder) {
				return ((DwcaBuilder) inputElement).getFileRoots().toArray();
			}
			if (inputElement instanceof DataSourceFile) {
				return ((DataSourceFile) inputElement).getSheets().toArray();
			}
			if (inputElement instanceof DataSourceSheet) {
				return new Object[] { ((DataSourceSheet) inputElement).getSheetName() };
			}
			return getChildren(inputElement);
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			_log.debug("getChildren " + parentElement + " " + parentElement.getClass());

			if (parentElement instanceof DwcaBuilder) {
				return ((DwcaBuilder) parentElement).getFileRoots().toArray();
			}
			if (parentElement instanceof DataSourceFile) {
				return ((DataSourceFile) parentElement).getSheets().toArray();
			}
			if (parentElement instanceof JdbcDataSource) {
				return ((JdbcDataSource) parentElement).getSheets().toArray();
			}
			if (parentElement instanceof DataSourceSheet) {
				return null;
			}
			if (parentElement instanceof JdbcDataSourceSheet) {
				return null;
			}
			return new Object[] { "Item 0", "Item 1", "Item 2" };
		}

		@Override
		public Object getParent(Object element) {
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			// _log.debug("hasChildren " + element + " " + element.getClass());

			if (element instanceof DwcaBuilder) {
				return ((DwcaBuilder) element).getFileRoots().size() > 0;
			}
			if (element instanceof DataSourceFile) {
				return ((DataSourceFile) element).getSheets().size() > 0;
			}
			if (element instanceof JdbcDataSource) {
				return ((JdbcDataSource) element).getSheets().size() > 0;
			}
			if (element instanceof DataSourceSheet) {
				return false;
			}
			if (element instanceof JdbcDataSourceSheet) {
				return false;
			}

			return getChildren(element).length > 0;
		}
	}

	private DataBindingContext m_bindingContext;
	protected Shell shlGenesysMagic;
	private MenuItem mntmQuit;
	private static Image archiveIcon;
	private static Image spreadsheetIcon;
	private static Image openPackageIcon;
	private static Image automapColumnsIcon;
	private static Image spreadsheetIcon16;
	static Image pushDataIcon;
	private static Image loadWorkspaceIcon;
	private static Image saveWorkspaceIcon;
	private static Image settingsIcon;
	private static Image addDatabaseIcon;
	static Image applicationIcon128;


	private Tree filesTree;
	private Display display;
	private TreeViewer treeViewer;
	private TableViewer tableViewer;
	private CTabFolder tabFolder;
	private Table table;
	
	static {
		AppWindow.archiveIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/archive.png")).createImage();
		AppWindow.openPackageIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/openpackage.png")).createImage();
		AppWindow.spreadsheetIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/spreadsheet.png")).createImage();
		AppWindow.spreadsheetIcon16 = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/spreadsheet16.png")).createImage();
		AppWindow.automapColumnsIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/automap.png")).createImage();
		AppWindow.pushDataIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/pushdata.png")).createImage();
		AppWindow.loadWorkspaceIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/load.png")).createImage();
		AppWindow.saveWorkspaceIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/save.png")).createImage();
		AppWindow.settingsIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/settings.png")).createImage();
		AppWindow.addDatabaseIcon = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/database-add.png")).createImage();
		AppWindow.applicationIcon128 = ImageDescriptor.createFromURL(AppWindow.class.getResource("/icon/appicon.png")).createImage();
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Shell workspaceShell = new Shell();
		
		WorkspaceDialog workspaceDialog = new WorkspaceDialog(workspaceShell);
		Object workspacePath = workspaceDialog.open();
		workspaceShell.dispose();

		if (workspacePath == null) {
			_log.error("No workspace selected. Exiting now!");
			return;
		}
		_log.info("Using workspace: " + workspacePath);
		System.setProperty("genesys.magic.workspace", (String) workspacePath);

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		AppWindow appWindow = (AppWindow) ctx.getBean("appWindow");
		appWindow.execute();

		_log.info("Closing Spring context");
		ctx.close();
		_log.info("All tidied up.");

		// Workaround for hanging derby.rawStoreDeamon
		System.exit(0);
	}

	private void execute() {
		Display display = Display.getDefault();
		final AppWindow that = this;
		Realm.runWithDefault(SWTObservables.getRealm(display), new Runnable() {
			@Override
			public void run() {
				try {
					that.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Open the window.
	 */
	public void open() {
		this.display = Display.getDefault();
		createContents();
		shlGenesysMagic.open();
		shlGenesysMagic.layout();

		while (!shlGenesysMagic.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		// Close thread pool
		threadPool.shutdown();
	}

	public static boolean isMac() {
		if (System.getProperty("os.name").equals("Mac OS X")) {
			return true;
		}
		return false;
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlGenesysMagic = new Shell();
		shlGenesysMagic.setMinimumSize(new Point(100, 22));
		shlGenesysMagic.setSize(744, 528);
		shlGenesysMagic.setText("Genesys Magic");
		shlGenesysMagic.setLayout(new BorderLayout(0, 0));
		shlGenesysMagic.setImages(new Image[] { applicationIcon128 });

		Menu menu = new Menu(shlGenesysMagic, SWT.BAR);
		shlGenesysMagic.setMenuBar(menu);

		MenuItem mntmFile = new MenuItem(menu, SWT.CASCADE);
		mntmFile.setText("File");

		Menu menu_1 = new Menu(mntmFile);
		mntmFile.setMenu(menu_1);

		MenuItem mntmOpen = new MenuItem(menu_1, SWT.NONE);
		mntmOpen.setText("Open");

		MenuItem mntmSettings = new MenuItem(menu_1, SWT.NONE);
		mntmSettings.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSettings();
			}
		});
		mntmSettings.setText("Settings ...");

		new MenuItem(menu_1, SWT.SEPARATOR);

		mntmQuit = new MenuItem(menu_1, SWT.NONE);
		mntmQuit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				_log.debug("Exiting now.");
				shlGenesysMagic.dispose();
			}
		});
		mntmQuit.setText("Quit");

		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");

		Menu menu_2 = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_2);

		MenuItem mntmAbout = new MenuItem(menu_2, SWT.NONE);
		mntmAbout.setText("About");

		new MenuItem(menu_2, SWT.SEPARATOR);

		MenuItem mntmVersion = new MenuItem(menu_2, SWT.NONE);
		mntmVersion.setText("Version...");

		SashForm sashForm_1 = new SashForm(shlGenesysMagic, SWT.VERTICAL);
		sashForm_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		sashForm_1.setLayoutData(BorderLayout.CENTER);

		SashForm sashForm = new SashForm(sashForm_1, SWT.NONE);

		treeViewer = new TreeViewer(sashForm, SWT.BORDER);
		treeViewer.setColumnProperties(new String[] {});
		filesTree = treeViewer.getTree();
		filesTree.setLinesVisible(true);

		Menu filesTreeMenu = new Menu(filesTree);
		filesTree.setMenu(filesTreeMenu);
		treeViewer.setLabelProvider(new ViewerLabelProvider());
		treeViewer.setContentProvider(new TreeContentProvider());
		treeViewer.setInput(builder);

		final MenuItem mntmAddFile = new MenuItem(filesTreeMenu, SWT.NONE);
		mntmAddFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Object selectedObject = ((IStructuredSelection) treeViewer.getSelection()).getFirstElement();
				if (selectedObject instanceof JdbcDataSource) {
					addJdbcQuery((JdbcDataSource) selectedObject);
				}
				if (selectedObject instanceof DataSourceFile) {
					addSourceFile();
				}
			}
		});

		filesTreeMenu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				Object selectedObject = ((IStructuredSelection) treeViewer.getSelection()).getFirstElement();
				if (selectedObject instanceof DataSourceFile) {
					mntmAddFile.setText("Add file");
				}
				if (selectedObject instanceof JdbcDataSource) {
					mntmAddFile.setText("Add SQL Query");
				}
			}
		});

		final MenuItem mntmRemoveFile = new MenuItem(filesTreeMenu, SWT.NONE);
		mntmRemoveFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				for (Iterator<?> iterator = ((IStructuredSelection) treeViewer.getSelection()).iterator(); iterator.hasNext();) {

					Object selectedObject = iterator.next();
					_log.debug("Remove Sel " + selectedObject);
					if (selectedObject instanceof IDataSource) {
						builder.removeDataSource((IDataSource) selectedObject);
						tableViewer.refresh();
					}
                    if (selectedObject instanceof JdbcDataSourceSheet) {
                        IStructuredSelection thisSelection = (IStructuredSelection) treeViewer.getSelection();
                        IDataSource currentSource= (IDataSource) ((TreeSelection) thisSelection).getPaths()[0].getFirstSegment();
                        if (confirmRemoveQuery()){
                            currentSource.removeSheet((IDataSourceSheet) selectedObject);
                        }
                        tableViewer.refresh();
                    }
				}

				treeViewer.refresh();
			}
		});
		mntmRemoveFile.setText("Remove");

		filesTreeMenu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				boolean enabled = false;
				for (Iterator<?> iterator = ((IStructuredSelection) treeViewer.getSelection()).iterator(); !enabled && iterator.hasNext();) {

					Object selectedObject = iterator.next();
					_log.debug("Sel " + selectedObject);
					if (selectedObject instanceof IDataSource) {
                        enabled = true;
                    }
                    if(selectedObject instanceof JdbcDataSourceSheet){
                        enabled=true;
                    }

				}
				mntmRemoveFile.setEnabled(enabled);
			}

		});
		final TreeColumn trclmnDataSource = new TreeColumn(filesTree, SWT.NONE);
		trclmnDataSource.setWidth(343);
		trclmnDataSource.setText("Data source");
		filesTree.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				// Pack to get minimum size
				trclmnDataSource.pack();
				trclmnDataSource.setWidth(Math.max(trclmnDataSource.getWidth(), filesTree.getClientArea().width));
			}
		});

		tabFolder = new CTabFolder(sashForm, SWT.NONE);
		tabFolder.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));

		tableViewer = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		DragSource dragSource = new DragSource(table, DND.DROP_LINK);
		dragSource.setTransfer(new Transfer[] { LocalSelectionTransfer.getTransfer() });

		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnPreferredName = tableViewerColumn.getColumn();
		tblclmnPreferredName.setWidth(100);
		tblclmnPreferredName.setText("Preferred name");

		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnTerm = tableViewerColumn_2.getColumn();
		tblclmnTerm.setWidth(100);
		tblclmnTerm.setText("Term");

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnRdfTerm = tableViewerColumn_1.getColumn();
		tblclmnRdfTerm.setWidth(100);
		tblclmnRdfTerm.setText("RDF URL");

		table.pack();

		dragSource.addDragListener(new DragSourceAdapter() {
			@Override
			public void dragStart(DragSourceEvent event) {
				_log.debug("Starting drag");
				event.doit = true;
				LocalSelectionTransfer.getTransfer().setSelection(tableViewer.getSelection());
			}
		});
		sashForm.setWeights(new int[] { 3, 6, 3 });

		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent e) {
				IStructuredSelection thisSelection = (IStructuredSelection) e.getSelection();
				Object selectedNode = thisSelection.getFirstElement();

                IDataSource currentSource= (IDataSource) ((TreeSelection) thisSelection).getPaths()[0].getFirstSegment();

                if (selectedNode instanceof IDataSourceSheet) {
					final IDataSourceSheet currentSheet = (IDataSourceSheet) selectedNode;
					_log.debug("Doubleclick " + currentSheet);
					int i = 0;
					for (Control c : tabFolder.getTabList()) {
						_log.debug("Looking at " + c);
						if (c instanceof SheetDisplay) {
							if (selectedNode == ((SheetDisplay) c).getDataSourceSheet()) {
								tabFolder.setSelection(i);
								return;
							}
							i++;
						}
					}
					CTabItem newSheetTab = createTab(tabFolder,currentSource, currentSheet);

					// Load stuff
					if (selectedNode instanceof DataSourceSheet) {

						threadPool.execute(new Runnable() {

							@Override
							public void run() {
								try {
									final List<Object[]> rows = dataSourceLoader.loadRows(currentSheet, 200);
									currentSheet.updateData(rows);

								} catch (UnsupportedDataFormatException e) {
									e.printStackTrace();
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						});
					}
					tabFolder.setSelection(newSheetTab);
				}
			}
		});
		sashForm_1.setWeights(new int[] { 4 });

		ToolBar toolBar = new ToolBar(shlGenesysMagic, SWT.FLAT);
		toolBar.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		toolBar.setLayoutData(BorderLayout.NORTH);

		ToolItem loadWorkSpace = new ToolItem(toolBar, SWT.NONE);
		loadWorkSpace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doLoadWorkSpace();
			}
		});
		loadWorkSpace.setText("Load");
		loadWorkSpace.setImage(loadWorkspaceIcon);

		ToolItem saveWorkSpace = new ToolItem(toolBar, SWT.NONE);
		saveWorkSpace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSaveWorkSpace();
			}
		});
		saveWorkSpace.setText("Save");
		saveWorkSpace.setImage(saveWorkspaceIcon);

		ToolItem tltmSettings = new ToolItem(toolBar, SWT.NONE);
		tltmSettings.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSettings();
			}
		});
		tltmSettings.setText("Settings");
		tltmSettings.setImage(settingsIcon);

		ToolItem tltmAddFile = new ToolItem(toolBar, SWT.NONE);
		tltmAddFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addSourceFile();
			}
		});
		tltmAddFile.setText("Add file");
		tltmAddFile.setImage(openPackageIcon);

		ToolItem tltmAutomap = new ToolItem(toolBar, SWT.NONE);
		tltmAutomap.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doAutomap();
			}
		});
		tltmAutomap.setText("Automap");
		tltmAutomap.setImage(automapColumnsIcon);

		ToolItem tltmPush = new ToolItem(toolBar, SWT.NONE);
		tltmPush.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doPush();
			}
		});
		tltmPush.setText("Push");
		tltmPush.setImage(pushDataIcon);

		ToolItem tltmDatabase = new ToolItem(toolBar, SWT.NONE);
		tltmDatabase.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doDatabase();
			}
		});
		tltmDatabase.setText("Add database");
		tltmDatabase.setImage(AppWindow.addDatabaseIcon);
		m_bindingContext = initDataBindings();
	}

	protected CTabItem createTab(CTabFolder tabFolder, IDataSource currentSource, IDataSourceSheet currentSheet) {

		CTabItem tbtmOther = new CTabItem(tabFolder, SWT.NONE);
		tbtmOther.setShowClose(true);
		tbtmOther.setText(currentSheet.getSheetName());
		tbtmOther.setImage(spreadsheetIcon16);

		final SheetDisplay sheetDisplay = (SheetDisplay) applicationContext.getBean("sheetDisplay",currentSource, currentSheet, tabFolder, treeViewer, SWT.NONE);
		tbtmOther.setControl(sheetDisplay);

		tbtmOther.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				sheetDisplay.dispose();
			}
		});

		return tbtmOther;
	}

    private boolean confirmRemoveQuery() {
        MessageBox dialog = new MessageBox(shlGenesysMagic, SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
        dialog.setText("Remove query");
        dialog.setMessage("Do you want remove this query?");
        return dialog.open() == SWT.OK;
    }

    private void doSaveWorkSpace() {
		FileDialog dialog = new FileDialog(shlGenesysMagic, SWT.SAVE);
		dialog.setFilterNames(new String[] { "JSON Format" });
		dialog.setFilterExtensions(new String[] { "*.json" });
		if (builder.getWorkspaceFilename() == null) {
			dialog.setFileName("workspace.json");
		} else {
			File file = new File(builder.getWorkspaceFilename());
			dialog.setFileName(file.getName());
			dialog.setFilterPath(file.getParent());
		}
		String path = dialog.open();

		if (path != null) {
			try {
				builder.saveWorkspace(path, settings);
			} catch (JSONException e) {
				_log.error(e.getMessage());
			} catch (IOException e) {
				_log.error(e.getMessage());
			}
		}
	}

	private void doLoadWorkSpace() {
		FileDialog dialog = new FileDialog(shlGenesysMagic, SWT.MULTI);
		dialog.setFilterNames(new String[] { "JSON Format" });
		dialog.setFilterExtensions(new String[] { "*.json" });
		String path = dialog.open();

		if (path != null) {
			try {
				builder.loadWorkspace(path, settings);
			} catch (FileNotFoundException e) {
				_log.error(e.getMessage());
			} catch (JSONException e) {
				_log.error(e.getMessage());
			}
		}
		treeViewer.refresh();
	}

	protected void doPush() {
		final IDataSourceSheet dss = getCurrentDataSourceSheet();
		if (dss == null) {
			return;
		}
        final IDataSource dataSource = getCurrentDataSource();


		PushDialog pushDialog = (PushDialog) applicationContext.getBean("pushDialog", shlGenesysMagic, SWT.NONE);
		pushDialog.setDataSourceSheet(dss);
        pushDialog.setDataSource(dataSource);
		pushDialog.setColumnDefs(this.columnDefs);

		pushDialog.open();
	}

	protected void doSettings() {
		SettingsDialog settingsDialog = (SettingsDialog) applicationContext.getBean("settingsDialog", shlGenesysMagic, SWT.NONE);
		settingsDialog.open();
	}

	protected void doDatabase() {
		DatabaseDialog databaseDialog = (DatabaseDialog) applicationContext.getBean("databaseDialog", shlGenesysMagic, treeViewer, SWT.NONE);
		databaseDialog.open();
	}

	protected void doAutomap() {
		final IDataSourceSheet dss = getCurrentDataSourceSheet();
		if (dss == null) {
			return;
		}

		_log.info("Got data source sheet " + dss);

		threadPool.execute(new Runnable() {
			@Override
			public void run() {
				dss.automap(columnDefs);
			}
		});
	}

	public IDataSourceSheet getCurrentDataSourceSheet() {
		CTabItem currentTab = tabFolder.getSelection();
		if (currentTab == null) {
			_log.info("No tab selected");
			return null;
		}
		final IDataSourceSheet dss = ((SheetDisplay) currentTab.getControl()).getDataSourceSheet();
		return dss;
	}

    public IDataSource getCurrentDataSource() {
        CTabItem currentTab = tabFolder.getSelection();
        if (currentTab == null) {
            _log.info("No tab selected");
            return null;
        }
        final IDataSource ds = ((SheetDisplay) currentTab.getControl()).getDataSource();
        return ds;
    }

	public void addJdbcQuery(JdbcDataSource dataSource) {
		JdbcDataSourceSheet dataSourceSheet = new JdbcDataSourceSheet();
		dataSource.addSheet(dataSourceSheet);
		treeViewer.refresh();
	}

	public void addSourceFile() {
		FileDialog fd = new FileDialog(shlGenesysMagic, SWT.MULTI);
		fd.setFilterNames(new String[] { "Supported source files", "Excel files", "CSV files" });
		fd.setFilterExtensions(new String[] { "*.xlsx;*.xls;*.csv;*.tab", "*.xlsx;*.xls", "*.csv;*.tab" });
		String response = fd.open();

		if (response != null) {
			for (String file : fd.getFileNames()) {
				_log.debug(fd.getFilterPath() + " " + file);
				builder.addDataSource(dataSourceLoader.loadDataSource(new File(fd.getFilterPath(), file)));

				// Update binding!
				_log.debug("Updating models!");
				m_bindingContext.updateModels();
			}
		}
		treeViewer.refresh();
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
		IObservableMap[] observeMap = BeansObservables.observeMaps(listContentProvider.getKnownElements(), ColumnDef.class, new String[] { "preferredName", "title", "rdfTerm" });
		tableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMap));
		tableViewer.setContentProvider(listContentProvider);
		//
		IObservableList fileRootsBuilderObserveList = BeansObservables.observeList(Realm.getDefault(), columnDefs, "columnDefs");
		tableViewer.setInput(fileRootsBuilderObserveList);
		//
		return bindingContext;
	}
}
