package org.genesys2.anno.gui;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.LogManager;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

public class SwtLogAppender extends WriterAppender {

	private static final String DEFAULT_LAYOUT_PATTERN = "%d{yyyy-MM-dd HH:mm:ss} %t %-5p %c{1}:%L - %m%n";
	private Text text;
	private Display display;
	private int maxBufferSize = 100000;
	private StringBuffer logBuffer = new StringBuffer();
	private AtomicBoolean resetText = new AtomicBoolean(true);

	public SwtLogAppender(Text text2) {
		this.text = text2;
		this.text.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				close();
			}
		});
		text.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				if (resetText.getAndSet(false)) {
					text.setText(logBuffer.toString());
					text.setSelection(logBuffer.length());
				}
			}
		});
		display = text.getDisplay();
		LogManager.getRootLogger().addAppender(this);
		PatternLayout patternLayout = new org.apache.log4j.PatternLayout();
		patternLayout.setConversionPattern(DEFAULT_LAYOUT_PATTERN);
		setLayout(patternLayout);
	}

	public void setConversionPattern(String conversionPattern) {
		PatternLayout patternLayout = (PatternLayout) getLayout();
		patternLayout.setConversionPattern(conversionPattern);
	}

	@Override
	public void close() {
		LogManager.getRootLogger().removeAppender(this);
		this.text = null;
		this.display = null;
	}

	@Override
	public void append(LoggingEvent loggingEvent) {
		// System.err.println("append!");
		if (text == null || display == null) {
			return;
		}

		String[] throwableStr = loggingEvent.getThrowableStrRep();
		StringBuffer sb = new StringBuffer();
		if (throwableStr != null) {
			for (String thst : throwableStr) {
				sb.append(thst).append('\n');
			}
		}
		final String mes = layout.format(loggingEvent) + (throwableStr == null ? "" : sb.toString());
		logBuffer.append(mes);
		if (logBuffer.length() > maxBufferSize) {
			// System.err.println("CLEARING POOL len=" + logBuffer.length() +
			// " removing=" + (logBuffer.length() - maxBufferSize));
			logBuffer.replace(0, logBuffer.length() - maxBufferSize, "");
			int pos = logBuffer.indexOf("\n");
			if (pos >= 0) {
				logBuffer.replace(0, pos + 1, "");
			}
		}
		resetText.set(true);

		// System.err.println(mes);
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				if (text != null) {
					text.redraw();
				}
			}
		});
	}

}