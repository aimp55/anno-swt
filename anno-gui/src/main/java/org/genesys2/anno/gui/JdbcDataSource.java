package org.genesys2.anno.gui;

import org.genesys2.anno.model.DatabaseSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JdbcDataSource extends AbstractModelObject implements IDataSource {

	private DatabaseSettings settings;

	public JdbcDataSource(String name) {
		this.name = name;
	}

	private String name;
	private List<IDataSourceSheet> dataSourceSheets = new ArrayList<IDataSourceSheet>();

	@Override
	public String getFileName() {
		return this.name;
	}

	@Override
	public File getFile() {
		/* Not applicable for jdbc datasource */
		return null;
	}

	@Override
	public void setFile(File file) {
		/* Not applicable for jdbc datasource */
	}

	@Override
	public List<IDataSourceSheet> getSheets() {
		return dataSourceSheets;
	}

	@Override
	public void setSheets(List<IDataSourceSheet> sheets) {
		this.dataSourceSheets = sheets;
		firePropertyChange("sheets", null, sheets);
	}

    @Override
    public void removeSheet(IDataSourceSheet sourceSheet) {
        this.dataSourceSheets.remove(sourceSheet);
        firePropertyChange("sheets", null, null);
    }

    public void addSheet(JdbcDataSourceSheet sourceSheet) {
		dataSourceSheets.add(sourceSheet);
	}

	@Override
	public String toString() {
		return this.name;
	}

	public void setSettings(DatabaseSettings settings) {
		this.settings = settings;
	}

	public DatabaseSettings getSettings() {
		return settings;
	}
}
