/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DataSourceFile extends AbstractModelObject implements IDataSource {
	private File file;
	private List<IDataSourceSheet> sheets = new ArrayList<IDataSourceSheet>();

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public void setFile(File file) {
		this.file = file;
		firePropertyChange("fileName", null, file);
	}

	@Override
	public String getFileName() {
		return this.file == null ? null : this.file.getName();
	}

	@Override
	public List<IDataSourceSheet> getSheets() {
		return sheets;
	}

	@Override
	public void setSheets(List<IDataSourceSheet> sheets) {
		this.sheets = sheets;
		firePropertyChange("sheets", null, sheets);
	}

    @Override
    public void removeSheet(IDataSourceSheet sourceSheet){
        this.sheets.remove(sourceSheet);
    }

	@Override
	public String toString() {
		return file.getName();
	}
}
