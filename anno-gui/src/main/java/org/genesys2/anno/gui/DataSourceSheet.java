/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.parser.CsvDataSourceSheet;

public class DataSourceSheet extends AbstractModelObject implements IDataSourceSheet {
	private static final Logger _log = Logger.getLogger(DataSourceSheet.class);

	private File sourceFile;
	private String sheetName;
	private List<String> columnNames = new ArrayList<String>();
	private Map<String, String> columnTerms = new HashMap<String, String>();
	private boolean headersIncluded = true;
	private int headerRowIndex = 0;
	private List<Object[]> sampleData;
	private List<Column> columns = new ArrayList<Column>();

	public DataSourceSheet(File sourceFile) {
		this.sourceFile = new File(sourceFile.getAbsolutePath());
	}

	@Override
	public File getSourceFile() {
		return sourceFile;
	}

	@Override
	public String getSheetName() {
		return sheetName;
	}

	@Override
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
		firePropertyChange("sheetName", null, sheetName);
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
		firePropertyChange("columnNames", null, columnNames);
	}

	public Map<String, String> getColumnTerms() {
		return columnTerms;
	}

	public void setColumnTerms(Map<String, String> columnTerms) {
		this.columnTerms = columnTerms;
		firePropertyChange("columnTerms", null, columnTerms);
	}

	@Override
	public void setHeadersIncluded(boolean headersIncluded) {
		_log.debug("Set headers included :" + headersIncluded);
		this.headersIncluded = headersIncluded;
		firePropertyChange("headersIncluded", null, headersIncluded);
	}

	@Override
	public boolean isHeadersIncluded() {
		return headersIncluded;
	}

	@Override
	public int getHeaderRowIndex() {
		return headerRowIndex;
	}

	@Override
	public void setHeaderRowIndex(int headerRowIndex) {
		_log.debug("Set header row index:" + headerRowIndex);
		this.headerRowIndex = headerRowIndex;
		firePropertyChange("headerRowIndex", null, headerRowIndex);
	}

	@Override
	public void updateData(List<Object[]> rows) {
		int columnCount = 0;
		for (Object[] row : rows) {
			if (row != null)
				columnCount = Math.max(columnCount, row.length);
		}

		Object[] headers = null;

		if (headersIncluded) {
			this.sampleData = rows.subList(headerRowIndex + 1, rows.size());
			try {
				Object[] headerRow = rows.get(headerRowIndex);
				headers = headerRow;
			} catch (IndexOutOfBoundsException e) {

			}
		} else {
			this.sampleData = rows;
		}

		// Make sure we have the Columns in the list
		ensureColumns(columnCount, headers);

		_log.debug("Firing sampleData change");
		firePropertyChange("sampleData", null, this.sampleData);
	}

	private void ensureColumns(int columnCount, Object[] headerRow) {
		boolean changed = false;
		for (int i = columns.size() - 1; i >= 0 && i > columnCount; i--) {
			_log.debug("Removing extra columns");
			columns.remove(i);
			changed = true;
		}
		for (int i = columns.size(); i < columnCount; i++) {
			_log.debug("Adding column " + i);
			columns.add(new Column());
			changed = true;
		}

		for (int i = columns.size() - 1; i >= 0; i--) {
			String headerColumnName = (headerRow != null && headerRow.length > i ? String.valueOf(headerRow[i]) : null);
			Column c = columns.get(i);
			c.setPreferredName(headerColumnName == null ? "Column " + i : headerColumnName);
		}

		// fire change
		if (changed)
			firePropertyChange("columns", null, columns);
	}

	@Override
	public List<Object[]> getSampleData() {
		return sampleData;
	}

	@Override
	public List<Column> getColumns() {
		return this.columns;
	}

	@Override
	public void setColumns(List<Column> columns) {
		this.columns = columns;
		firePropertyChange("columns", null, this.columns);
	}

	public Column getColumn(int columnIndex) {
		return this.columns.get(columnIndex);
	}

	@Override
	public boolean isCsv() {
		System.err.println(this.getClass() + " " + (this instanceof CsvDataSourceSheet));
		return this instanceof CsvDataSourceSheet;
	}

	@Override
	public boolean isSqlQuery() {
		return false;
	}

	@Override
	public void automap(ColumnDefs columnDefs) {
		for (ColumnDef columnDef : columnDefs.getColumnDefs()) {
			_log.debug("Looking at " + columnDef.getPreferredName());
			for (Column column : columns) {
				if (StringUtils.isBlank(column.getRdfTerm())) {
					// Change only if blank
					if (columnDef.getPreferredName().equalsIgnoreCase(column.getPreferredName())) {
						column.setRdfTerm(columnDef.getRdfTerm());
						column.setMultiple(columnDef.hasAllowMultiple());
						column.setDescription(columnDef.getTitle());
					}
				}
			}
		}
	}

	@Override
	public String getQuery() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void setQuery(String query) {
		// Auto-generated method stub
	}

}
