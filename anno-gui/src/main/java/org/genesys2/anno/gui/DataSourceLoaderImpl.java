/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.parser.RowReader;
import org.genesys2.anno.reader.JDBCRowReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataSourceLoaderImpl implements DataSourceLoader {
	private static final Logger _log = Logger.getLogger(DataSourceLoaderImpl.class);

	private final List<DataSourceSheet> NO_SHEETS = new ArrayList<DataSourceSheet>();
	private Set<DataSourceParser> parsers = new HashSet<DataSourceParser>();

	@Autowired
	private JDBCRowReader jdbcRowReader;

	@Override
	public void registerParser(DataSourceParser parser) {
		_log.debug("Adding parser " + parser.getClass().getName());
		this.parsers.add(parser);
	}

	@Override
	public DataSourceFile loadDataSource(File file) {
		if (!file.exists() || !file.canRead()) {
			_log.debug("Could not find or read " + file.getAbsolutePath());
			return null;
		}
		DataSourceFile dataSourceFile = new DataSourceFile();
		dataSourceFile.setFile(file);

		dataSourceFile.getSheets().addAll(findSheets(dataSourceFile));

		return dataSourceFile;
	}

	private Collection<? extends DataSourceSheet> findSheets(DataSourceFile dataSourceFile) {

		File sourceFile = dataSourceFile.getFile();

		// Iterate over parsers and see if something comes out
		for (DataSourceParser parser : parsers) {
			try {
				if (parser.supports(sourceFile)) {
					return parser.findSheets(sourceFile);
				}
			} catch (UnsupportedDataFormatException e) {
				_log.debug(e.getMessage());
			}
		}

		return NO_SHEETS;

	}

	@Override
	public List<Object[]> loadRows(IDataSourceSheet sheet, int maxRows) throws UnsupportedDataFormatException, FileNotFoundException, IOException {
		// Iterate over parsers and see if something comes out
		for (DataSourceParser parser : parsers) {
			try {
				if (parser.supports(sheet.getSourceFile())) {
					return parser.loadRows(sheet, maxRows, 0);
				}
			} catch (UnsupportedDataFormatException e) {
				_log.debug(e.getMessage(), e);
			}
		}

		throw new UnsupportedDataFormatException("No parser found", null);
	}

	@Override
	public List<Object[]> loadDataRows(DataSourceSheet sheet, int maxRows) throws UnsupportedDataFormatException, FileNotFoundException, IOException {
		// Iterate over parsers and see if something comes out
		for (DataSourceParser parser : parsers) {
			try {
				if (parser.supports(sheet.getSourceFile())) {
					return parser.loadRows(sheet, maxRows, sheet.getHeaderRowIndex() + 1);
				}
			} catch (UnsupportedDataFormatException e) {
				_log.debug(e.getMessage(), e);
			}
		}

		throw new UnsupportedDataFormatException("No parser found", null);
	}

	@Override
	public RowReader createRowReader(IDataSourceSheet sheet, IDataSource dataSource) throws IOException, UnsupportedDataFormatException {

		if (sheet == null) {
			throw new RuntimeException("dataSourceSheet cannot be null");
		}

		// Iterate over parsers and see if something comes out

		if (sheet instanceof JdbcDataSourceSheet) {
			JdbcDataSourceSheet jdbcSheet = (JdbcDataSourceSheet) sheet;
			DatabaseSettings databaseSettings = ((JdbcDataSource) dataSource).getSettings();
			return jdbcRowReader.createRowReader(jdbcSheet.getQuery(), databaseSettings);
		}

		for (DataSourceParser parser : parsers) {
			try {
				if (parser.supports(sheet.getSourceFile())) {
					return parser.createRowReader(sheet);
				}
			} catch (UnsupportedDataFormatException e) {
				_log.debug(e.getMessage(), e);
			}
		}

		throw new UnsupportedDataFormatException("No parser found", null);
	}
}
