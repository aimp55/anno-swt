package org.genesys2.anno.gui;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class SwtUtil {

	public static void showMessageBox(final Shell shell, final String title, final String message) {
		shell.getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				MessageBox mb = new MessageBox(shell);
				mb.setMessage(StringUtils.defaultIfBlank(message, "?"));
				mb.setText(StringUtils.defaultIfBlank(title, "A message from our sponsors"));
				mb.open();
			}
		});
	}

	public static boolean showYesNoPrompt(final Shell shell, final String title, final String message) {
		MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.YES | SWT.NO);
		mb.setMessage(StringUtils.defaultIfBlank(message, "?"));
		mb.setText(StringUtils.defaultIfBlank(title, "A message from our sponsors"));
		return SWT.YES == mb.open();
	}

}
