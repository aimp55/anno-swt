package org.genesys2.anno.model;

import java.util.HashSet;
import java.util.Set;

import org.genesys2.anno.gui.AbstractModelObject;

public class ColumnDef extends AbstractModelObject {
	private String preferredName;
	private ColumnDataType dataType;
	private boolean unique;
	private String rdfTerm;
	private Set<ColumnValidator> validators = new HashSet<ColumnValidator>();
	private String title;
	private boolean allowMultiple = false;

	public ColumnDef() {
	}

	public ColumnDef(String preferredName, String title, ColumnDataType dataType, boolean unique, String rdfTerm) {
		this.preferredName = preferredName;
		this.title = title;
		this.dataType = dataType;
		this.unique = unique;
		this.rdfTerm = rdfTerm;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public ColumnDataType getDataType() {
		return dataType;
	}

	public void setDataType(ColumnDataType dataType) {
		this.dataType = dataType;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public String getRdfTerm() {
		return rdfTerm;
	}

	public void setRdfTerm(String rdfTerm) {
		this.rdfTerm = rdfTerm;
	}

	public Set<ColumnValidator> getValidators() {
		return validators;
	}

	public void setValidators(Set<ColumnValidator> validators) {
		this.validators = validators;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean hasAllowMultiple() {
		return this.allowMultiple;
	}

	public ColumnDef setAllowMultiple(boolean allowMultiple) {
		this.allowMultiple = allowMultiple;
		return this;
	}
}
