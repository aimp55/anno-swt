/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.model;

import org.genesys2.anno.gui.AbstractModelObject;

public class Column extends AbstractModelObject {
	private String preferredName;
	private String rdfTerm;
	private String description;
	private ColumnDataType dataType;
	private boolean unique;
	private boolean multiple;
	private String separator = ";";
	private String pattern;
	private String groupPattern;
	private boolean includeNull = false;

	public Column() {
	}

	public Column(String preferredName, ColumnDataType dataType, boolean multiple, boolean unique, String rdfTerm) {
		this.preferredName = preferredName;
		this.dataType = dataType;
		this.multiple = multiple;
		this.unique = unique;
		this.rdfTerm = rdfTerm;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
		firePropertyChange("preferredName", null, this.preferredName);
	}

	public String getRdfTerm() {
		return rdfTerm;
	}

	public void setRdfTerm(String rdfTerm) {
		this.rdfTerm = rdfTerm;
		firePropertyChange("rdfTerm", null, this.rdfTerm);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		firePropertyChange("description", null, this.description);
	}

	public ColumnDataType getDataType() {
		return dataType;
	}

	public void setDataType(ColumnDataType dataType) {
		this.dataType = dataType;
		firePropertyChange("dataType", null, this.dataType);
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
		firePropertyChange("unique", null, this.unique);
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
		firePropertyChange("multiple", null, this.multiple);
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
		firePropertyChange("pattern", null, this.pattern);
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
		firePropertyChange("separator", null, this.separator);
	}

	public String getGroupPattern() {
		return groupPattern;
	}

	public void setGroupPattern(String groupPattern) {
		this.groupPattern = groupPattern;
		firePropertyChange("groupPattern", null, this.groupPattern);
	}

	public void setIncludeNull(boolean includeNull) {
		this.includeNull = includeNull;
	}

	public boolean isIncludeNull() {
		return includeNull;
	}

	public boolean getIncludeNull() {
		return includeNull;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Column column = (Column) o;

		if (preferredName != null ? !preferredName.equals(column.preferredName) : column.preferredName != null)
			return false;

		return true;
	}

}
