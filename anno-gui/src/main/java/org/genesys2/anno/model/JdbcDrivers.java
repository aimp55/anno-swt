package org.genesys2.anno.model;


import org.genesys2.anno.gui.AbstractModelObject;

import java.util.ArrayList;
import java.util.List;

public class JdbcDrivers extends AbstractModelObject {


    private List<JdbcDriver> jdbcDrivers = new ArrayList<JdbcDriver>();

    private String mySqlDownloadUrl = "http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.18/mysql-connector-java-5.1.18.jar";
    private String msSqlDownloadUrl = "http://download1058.mediafire.com/8x31owqyfiog/3ycmaelhoot/sqljdbc4.jar";
    private String postgreSqlDownloadUrl = "http://central.maven.org/maven2/org/postgresql/postgresql/9.3-1102-jdbc41/postgresql-9.3-1102-jdbc41.jar";
    private String jdbcOdbcDownloadUrl = "";

    {
        jdbcDrivers.add(new JdbcDriver("MySQL", "com.mysql.jdbc.Driver", mySqlDownloadUrl, "jdbc:mysql://localhost:3306/DATABASE"));
        jdbcDrivers.add(new JdbcDriver("MsSQL", "com.microsoft.sqlserver.jdbc.SQLServerDriver", msSqlDownloadUrl, "jdbc:sqlserver://localhost\\INSTANCE:1433;databaseName=DATABASE"));
        jdbcDrivers.add(new JdbcDriver("PostgreSQL", "org.postgresql.Driver", postgreSqlDownloadUrl, "jdbc:postgresql://localhost:5432/DATABASE"));
        jdbcDrivers.add(new JdbcDriver("JDBC ODBC", "sun.jdbc.odbc.JdbcOdbcDriver", jdbcOdbcDownloadUrl, "jdbc:odbc://JDBC-ODBC-CONNECTSTRING"));

    }

    public List<JdbcDriver> getJdbcDrivers() {
        return jdbcDrivers;
    }

    public void setJdbcDrivers(List<JdbcDriver> jdbcDrivers) {
        this.jdbcDrivers = jdbcDrivers;
    }

    public void addJdbcDriver(JdbcDriver driver) {
        jdbcDrivers.add(driver);
    }
}
