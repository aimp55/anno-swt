package org.genesys2.anno.model;

import org.genesys2.anno.gui.AbstractModelObject;

public class OAuthSettings extends AbstractModelObject {
	private String serverUrl = "http://localhost:8080";
	private String authorizationEndpoint = "http://localhost:8080/oauth/authorize";
	private String tokenEndpoint = "http://localhost:8080/oauth/token";
	private String apiUrl = "http://localhost:8080/api/v0";
	private String accessToken = "";
	private String refreshToken = "";
	private String clientKey = "";
	private String clientSecret = "";

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
		setAuthorizationEndpoint(this.serverUrl + "/oauth/authorize");
		setTokenEndpoint(this.serverUrl + "/oauth/token");
		setApiUrl(this.serverUrl + "/api/v0");
		firePropertyChange("serverUrl", null, this.serverUrl);
	}

	public String getAuthorizationEndpoint() {
		return authorizationEndpoint;
	}

	public void setAuthorizationEndpoint(String authorizationEndpoint) {
		this.authorizationEndpoint = authorizationEndpoint;
		firePropertyChange("authorizationEndpoint", null, this.authorizationEndpoint);
	}

	public String getTokenEndpoint() {
		return tokenEndpoint;
	}

	public void setTokenEndpoint(String tokenEndpoint) {
		this.tokenEndpoint = tokenEndpoint;
		firePropertyChange("tokenEndpoint", null, this.tokenEndpoint);
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
		firePropertyChange("apiUrl", null, this.apiUrl);
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		firePropertyChange("accessToken", null, this.accessToken);
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
		firePropertyChange("refreshToken", null, this.refreshToken);
	}

	public void clearTokens() {
		setAccessToken("");
		setRefreshToken("");
	}

	public String getClientKey() {
		return clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
		firePropertyChange("clientKey", null, this.clientKey);
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
		firePropertyChange("clientSecret", null, this.clientSecret);
	}

}
