package org.genesys2.anno.model;

import org.genesys2.anno.gui.AbstractModelObject;

public class Settings extends AbstractModelObject {
	private OAuthSettings oauthSettings = new OAuthSettings();

	public OAuthSettings getOauthSettings() {
		return oauthSettings;
	}

	public void setOauthSettings(OAuthSettings oauthSettings) {
		this.oauthSettings = oauthSettings;
		firePropertyChange("oauthSettings", null, this.oauthSettings);
	}
}
