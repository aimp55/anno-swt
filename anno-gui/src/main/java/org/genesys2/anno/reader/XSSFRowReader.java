/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/* ====================================================================
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ==================================================================== */

package org.genesys2.anno.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFReader.SheetIterator;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.genesys2.anno.reader.MyXSSFSheetHandler.RowHandler;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * File for HSSF testing/examples
 * 
 * THIS IS NOT THE MAIN HSSF FILE!! This is a utility for testing functionality.
 * It does contain sample API usage that may be educational to regular API
 * users.
 * 
 * @see #main
 * @author Andrew Oliver (acoliver at apache dot org)
 */
public final class XSSFRowReader {
	private static final Logger _log = Logger.getLogger(XSSFRowReader.class);

	/**
	 * The type of the data value is indicated by an attribute on the cell. The
	 * value is usually in a "v" element within the cell.
	 */
	enum xssfDataType {
		BOOL, ERROR, FORMULA, INLINESTR, SSTINDEX, NUMBER,
	}

	public static void processAllSheets(String filename) throws Exception {
		OPCPackage pkg = OPCPackage.open(filename, PackageAccess.READ);
		try {
			XSSFReader r = new XSSFReader(pkg);
			SharedStringsTable sst = r.getSharedStringsTable();

			ContentHandler handler = new SheetHandler(sst);
			XMLReader parser = fetchSheetParser(handler);

			XSSFReader.SheetIterator sheets = (SheetIterator) r.getSheetsData();
			while (sheets.hasNext()) {
				InputStream sheet = sheets.next();
				System.out.println("Processing sheet: '" + sheets.getSheetName() + "'");

				try {
					// InputSource sheetSource = new InputSource(sheet);
					// parser.parse(sheetSource);
				} finally {
					_log.debug("Closing sheet");
					sheet.close();
					System.out.println("");
				}
			}

		} finally {
			_log.debug("Closing OPCPackage, no save");
			pkg.revert();
			pkg.close();
		}
	}

	public static XMLReader fetchSheetParser(ContentHandler handler) throws SAXException {
		XMLReader parser = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		parser.setContentHandler(handler);
		return parser;
	}

	/**
	 * See org.xml.sax.helpers.DefaultHandler javadocs
	 */
	private static class SheetHandler extends DefaultHandler {
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		private int currentRow = 0;

		private SheetHandler(SharedStringsTable sst) {
			this.sst = sst;
		}

		@Override
		public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
			// System.out.println(">> " + name);
			// row => row
			if (name.equals("row")) {
				currentRow++;
				if (currentRow == 100) {
					throw new StopParsingException("Stopping parse after " + currentRow + " rows");
				}
				System.out.println("NEW ROW------- " + currentRow);
			}
			// c => cell
			else if (name.equals("c")) {
				// Print the cell reference
				System.out.print(attributes.getValue("r") + " - ");

				// for (int i=attributes.getLength()-1; i>=0; i--)
				// _log.debug(attributes.getLocalName(i) + "=" +
				// attributes.getValue(i));

				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if (cellType != null && cellType.equals("s")) {
					nextIsString = true;
				} else {
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}

		@Override
		public void endElement(String uri, String localName, String name) throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if (nextIsString) {
				int idx = Integer.parseInt(lastContents);
				lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
				nextIsString = false;
			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if (name.equals("v")) {
				System.out.println(lastContents);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}

	public static List<Object[]> getRows(String filename, int maxRows) throws IOException, SAXException, OpenXML4JException {
		OPCPackage pkg = OPCPackage.open(filename, PackageAccess.READ);
		try {
			XSSFReader r = new XSSFReader(pkg);
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(pkg);
			StylesTable styles = r.getStylesTable();
			MyXSSFSheetHandler handler = new MyXSSFSheetHandler(styles, strings, 0, maxRows);
			XMLReader parser = fetchSheetParser(handler);

			XSSFReader.SheetIterator sheets = (SheetIterator) r.getSheetsData();
			while (sheets.hasNext()) {
				final List<Object[]> rows = new ArrayList<Object[]>();
				handler.setRowHandler(new RowHandler() {
					@Override
					public void handleRow(Object[] row) {
						rows.add(row);
					}
				});
				InputStream sheet = sheets.next();
				System.out.println("Processing sheet: '" + sheets.getSheetName() + "'");

				try {
					InputSource sheetSource = new InputSource(sheet);
					parser.parse(sheetSource);

				} catch (StopParsingException e) {
					// NOP
				} finally {
					_log.debug("Closing sheet");
					sheet.close();
					System.out.println("");
				}
				return rows;
			}

		} finally {
			_log.debug("Closing OPCPackage");
			pkg.revert();
			pkg.close();
		}

		return null;
	}

	/**
	 * See org.xml.sax.helpers.DefaultHandler javadocs
	 */
	private static class SheetRowHandler extends DefaultHandler {
		private static final String[] STRING_ARRAY = new String[] {};
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		private int currentRow = 0;
		private int maxRows;
		private RowHandler rowHandler = null;
		private List<String> row = new ArrayList<String>(100);

		private SheetRowHandler(SharedStringsTable sst, int maxRows) {
			this.sst = sst;
			this.maxRows = maxRows;
		}

		public void setRowHandler(RowHandler rowHandler) {
			this.rowHandler = rowHandler;
		}

		@Override
		public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
			// System.out.println(">> " + name);
			// row => row
			if (name.equals("row")) {
				row.clear();
				currentRow++;
				if (currentRow >= maxRows) {
					throw new StopParsingException("Stopping parse after " + currentRow + " rows");
				}
				System.out.println("NEW ROW------- " + currentRow);
			}
			// c => cell
			else if (name.equals("c")) {
				// Print the cell reference
				System.out.print(attributes.getValue("r") + " - ");

				int column = nameToColumn(attributes.getValue("r"));
				for (int i = row.size(); i < column; i++) {
					row.add(null);
				}

				// for (int i=attributes.getLength()-1; i>=0; i--)
				// _log.debug(attributes.getLocalName(i) + "=" +
				// attributes.getValue(i));

				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if (cellType != null && cellType.equals("s")) {
					nextIsString = true;
				} else {
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}

		@Override
		public void endElement(String uri, String localName, String name) throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if (nextIsString) {
				int idx = Integer.parseInt(lastContents);
				lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
				nextIsString = false;
			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if (name.equals("v")) {
				System.out.println(lastContents);
				row.add(lastContents);
			}

			if (name.equals("row")) {
				// Row ends
				System.out.println("Row end----");
				if (this.rowHandler != null)
					try {
						rowHandler.handleRow(row.toArray(STRING_ARRAY));
					} catch (InterruptedException e) {
						throw new StopParsingException(e.getMessage());
					}
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}

	/**
	 * Converts an Excel column name like "C" to a zero-based index.
	 * 
	 * @param name
	 * @return Index corresponding to the specified name
	 */
	public static int nameToColumn(String name) {
		int column = -1;
		for (int i = 0; i < name.length(); ++i) {
			int c = name.charAt(i);
			if (c >= 'A' && c <= 'Z')
				column = (column + 1) * 26 + c - 'A';
		}
		return column;
	}
}
