/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.reader;

import org.xml.sax.SAXException;

public class StopParsingException extends SAXException {
	private static final long serialVersionUID = -2930895184496310303L;

	public StopParsingException(String message) {
		super(message);
	}

	public StopParsingException(String message, Throwable e) {
		super(message);
	}

}
