/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.reader;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.genesys2.anno.reader.XSSFRowReader.xssfDataType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Derived from https://svn.apache.org/repos/asf/poi/trunk/src/examples/src/org
 * /apache/poi/xssf/eventusermodel/XLSX2CSV.java Derived from
 * http://poi.apache.org/spreadsheet/how-to.html#xssf_sax_api
 */
public class MyXSSFSheetHandler extends DefaultHandler {

	public static interface RowHandler {
		public void handleRow(Object[] objects) throws StopParsingException, InterruptedException;
	}

	/**
	 * Table with styles
	 */
	private StylesTable stylesTable;

	/**
	 * Table with unique strings
	 */
	private ReadOnlySharedStringsTable sharedStringsTable;

	/**
	 * Number of columns to read starting with leftmost
	 */
	private final int minColumnCount;

	// Set when V start element is seen
	private boolean vIsOpen;

	// Set when cell start element is seen;
	// used when cell close element is seen.
	private xssfDataType nextDataType;

	// Used to format numeric cell values.
	private short formatIndex;
	private String formatString;

	private int thisColumn = -1;
	// The last column printed to the output stream
	private int lastColumnNumber = -1;

	// Gathers characters as they are seen.
	private StringBuffer value;

	private int minColumns = -1;

	private RowHandler rowHandler;

	private List<Object> row = new ArrayList<Object>(100);

	private int currentRow = 0;

	private int maxRows;

	public MyXSSFSheetHandler(StylesTable styles, ReadOnlySharedStringsTable strings, int cols) {
		this.stylesTable = styles;
		this.sharedStringsTable = strings;
		this.minColumnCount = cols;
		this.value = new StringBuffer();
		this.nextDataType = xssfDataType.NUMBER;
		this.maxRows = -1;
	}

	/**
	 * Accepts objects needed while parsing.
	 * 
	 * @param styles
	 *            Table of styles
	 * @param strings
	 *            Table of shared strings
	 * @param cols
	 *            Minimum number of columns to show
	 * @param target
	 *            Sink for output
	 */
	public MyXSSFSheetHandler(StylesTable styles, ReadOnlySharedStringsTable strings, int cols, int maxRows) {
		this.stylesTable = styles;
		this.sharedStringsTable = strings;
		this.minColumnCount = cols;
		this.value = new StringBuffer();
		this.nextDataType = xssfDataType.NUMBER;
		this.maxRows = maxRows;
	}

	public void setRowHandler(RowHandler rowHandler) {
		this.rowHandler = rowHandler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String,
	 * java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {

		if ("inlineStr".equals(name) || "v".equals(name)) {
			vIsOpen = true;
			// Clear contents cache
			value.setLength(0);
		}
		// c => cell
		else if ("c".equals(name)) {
			// Get the cell reference
			String r = attributes.getValue("r");
			int firstDigit = -1;
			for (int c = 0; c < r.length(); ++c) {
				if (Character.isDigit(r.charAt(c))) {
					firstDigit = c;
					break;
				}
			}
			thisColumn = nameToColumn(r.substring(0, firstDigit));

			// Set up defaults.
			this.nextDataType = xssfDataType.NUMBER;
			this.formatIndex = -1;
			this.formatString = null;
			String cellType = attributes.getValue("t");
			String cellStyleStr = attributes.getValue("s");
			if ("b".equals(cellType))
				nextDataType = xssfDataType.BOOL;
			else if ("e".equals(cellType))
				nextDataType = xssfDataType.ERROR;
			else if ("inlineStr".equals(cellType))
				nextDataType = xssfDataType.INLINESTR;
			else if ("s".equals(cellType))
				nextDataType = xssfDataType.SSTINDEX;
			else if ("str".equals(cellType))
				nextDataType = xssfDataType.FORMULA;
			else if (cellStyleStr != null) {
				// It's a number, but almost certainly one
				// with a special style or format
				int styleIndex = Integer.parseInt(cellStyleStr);
				XSSFCellStyle style = stylesTable.getStyleAt(styleIndex);
				this.formatIndex = style.getDataFormat();
				this.formatString = style.getDataFormatString();
				if (this.formatString == null)
					this.formatString = BuiltinFormats.getBuiltinFormat(this.formatIndex);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {

		Object thisObj = null;

		// v => contents of a cell
		if ("v".equals(name)) {
			// Process the value contents as required.
			// Do now, as characters() may be called more than once
			switch (nextDataType) {

			case BOOL:
				char first = value.charAt(0);
				thisObj = first == '0' ? false : true;
				break;

			case ERROR:
				thisObj = "\"ERROR:" + value.toString() + '"';
				break;

			case FORMULA:
				// A formula could result in a string value,
				// so always add double-quote characters.
				thisObj = value.toString();
				break;

			case INLINESTR:
				// TODO: have seen an example of this, so it's untested.
				XSSFRichTextString rtsi = new XSSFRichTextString(value.toString());
				thisObj = rtsi.toString(); // '"' + rtsi.toString() + '"';
				break;

			case SSTINDEX:
				String sstIndex = value.toString();
				try {
					int idx = Integer.parseInt(sstIndex);
					XSSFRichTextString rtss = new XSSFRichTextString(sharedStringsTable.getEntryAt(idx));
					thisObj = rtss.toString(); // '"' + rtss.toString() +
												// '"';
				} catch (NumberFormatException ex) {
					System.err.println("Failed to parse SST index '" + sstIndex + "': " + ex.toString());
				}
				break;

			case NUMBER:
				String n = value.toString();
				if (n.contains("."))
					thisObj = Double.parseDouble(n);
				else
					thisObj = Long.parseLong(n);
				// if (this.formatString != null)
				// thisObj = d;
				// // formatter.formatRawCellContents(d,
				// // this.formatIndex,this.formatString);
				// else
				// thisObj = d;
				break;

			default:
				thisObj = "(TODO: Unexpected type: " + nextDataType + ")";
				break;
			}

			// Output after we've seen the string contents
			// Emit commas for any fields that were missing on this row
			if (lastColumnNumber == -1) {
				lastColumnNumber = 0;
			}
			for (int i = lastColumnNumber; i < thisColumn - 1; ++i)
				row.add(null);

			// Might be the empty string.
			row.add(thisObj);

			// Update column
			if (thisColumn > -1)
				lastColumnNumber = thisColumn;

		} else if ("row".equals(name)) {

			// Print out any missing commas if needed
			if (minColumns > 0) {
				// Columns are 0 based
				if (lastColumnNumber == -1) {
					lastColumnNumber = 0;
				}
				for (int i = lastColumnNumber; i < (this.minColumnCount); i++) {
					row.add(null);
				}
			}

			// We're onto a new row
			if (this.rowHandler != null)
				try {
					rowHandler.handleRow(row.toArray());
				} catch (Throwable e) {
					throw new StopParsingException(e.getMessage(), e);
				}

			row.clear();
			lastColumnNumber = -1;

			currentRow++;
			if (maxRows >= 0 && currentRow >= maxRows) {
				throw new StopParsingException("Stopping parse after " + currentRow + " rows");
			}
		}

	}

	/**
	 * Captures characters only if a suitable element is open. Originally was
	 * just "v"; extended for inlineStr also.
	 */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (vIsOpen)
			value.append(ch, start, length);
	}

	/**
	 * Converts an Excel column name like "C" to a zero-based index.
	 * 
	 * @param name
	 * @return Index corresponding to the specified name
	 */
	private int nameToColumn(String name) {
		int column = -1;
		for (int i = 0; i < name.length(); ++i) {
			int c = name.charAt(i);
			column = (column + 1) * 26 + c - 'A';
		}
		return column;
	}

}
