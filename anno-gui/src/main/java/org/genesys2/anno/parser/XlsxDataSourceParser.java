/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.parser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFReader.SheetIterator;
import org.apache.poi.xssf.model.StylesTable;
import org.genesys2.anno.gui.DataSourceSheet;
import org.genesys2.anno.gui.IDataSourceSheet;
import org.genesys2.anno.gui.UnsupportedDataFormatException;
import org.genesys2.anno.reader.MyXSSFSheetHandler;
import org.genesys2.anno.reader.MyXSSFSheetHandler.RowHandler;
import org.genesys2.anno.reader.StopParsingException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class XlsxDataSourceParser extends BasicDataSourceParser {
	private static final Logger _log = Logger.getLogger(XlsxDataSourceParser.class);

	private final String[] supportedExtensions = new String[] { ".xlsx" };

	@Override
	public String[] getSupportedExtensions() {
		return supportedExtensions;
	}

	@Override
	public Collection<? extends DataSourceSheet> findSheets(File sourceFile) throws UnsupportedDataFormatException {
		ArrayList<DataSourceSheet> sheets = new ArrayList<DataSourceSheet>();

		OPCPackage pkg;
		try {
			pkg = OPCPackage.open(sourceFile, PackageAccess.READ);
		} catch (InvalidFormatException e) {
			throw new UnsupportedDataFormatException("Could not open " + sourceFile.getAbsolutePath(), e);
		}

		try {
			XSSFReader r = new XSSFReader(pkg);

			XSSFReader.SheetIterator sheetsIterator = (SheetIterator) r.getSheetsData();
			while (sheetsIterator.hasNext()) {
				InputStream sheet = sheetsIterator.next();
				System.out.println("Processing sheet: '" + sheetsIterator.getSheetName() + "'");

				try {
					DataSourceSheet sheetDataSource = new DataSourceSheet(sourceFile);
					sheetDataSource.setSheetName(sheetsIterator.getSheetName());
					sheets.add(sheetDataSource);
				} finally {
					_log.debug("Closing sheet");
					sheet.close();
					System.out.println("");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new UnsupportedDataFormatException("Could not open " + sourceFile.getAbsolutePath(), e);

		} finally {
			_log.debug("Closing OPCPackage, no save");
			pkg.revert();
			try {
				pkg.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return sheets;
	}

	private XMLReader fetchSheetParser(ContentHandler handler) throws SAXException {
		XMLReader parser = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		parser.setContentHandler(handler);
		return parser;
	}

	@Override
	public List<Object[]> loadRows(IDataSourceSheet dataSourceSheet, int maxRows, final int startAt) throws UnsupportedDataFormatException {

		File sourceFile = dataSourceSheet.getSourceFile();
		String sheetName = dataSourceSheet.getSheetName();

		OPCPackage pkg;
		try {
			pkg = OPCPackage.open(sourceFile, PackageAccess.READ);
		} catch (InvalidFormatException e) {
			throw new UnsupportedDataFormatException("Could not open " + sourceFile.getAbsolutePath(), e);
		}

		try {
			XSSFReader r = new XSSFReader(pkg);
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(pkg);
			StylesTable styles = r.getStylesTable();
			MyXSSFSheetHandler handler = new MyXSSFSheetHandler(styles, strings, 0, maxRows);
			XMLReader parser = fetchSheetParser(handler);

			XSSFReader.SheetIterator sheetsIterator = (SheetIterator) r.getSheetsData();
			while (sheetsIterator.hasNext()) {
				InputStream sheet = sheetsIterator.next();
				System.out.println("Found sheet: '" + sheetsIterator.getSheetName() + "'");

				if (sheetName.equals(sheetsIterator.getSheetName())) {
					_log.debug("Found sheet.");

					final List<Object[]> rows = new ArrayList<Object[]>();
					handler.setRowHandler(new RowHandler() {
						int rowCount = 0;

						@Override
						public void handleRow(Object[] row) {
							if (startAt > rowCount) {
								return;
							}
							rowCount++;
							rows.add(row);
						}
					});

					try {
						InputSource sheetSource = new InputSource(sheet);
						parser.parse(sheetSource);

					} catch (StopParsingException e) {
						// NOP
					} finally {
						_log.debug("Closing sheet");
						sheet.close();
						System.out.println("");
					}
					return rows;
				}
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (OpenXML4JException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SAXException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			_log.debug("Closing OPCPackage");
			pkg.revert();
			try {
				pkg.close();
			} catch (IOException e) {
				_log.error(e.getMessage(), e);
			}
		}

		throw new UnsupportedDataFormatException("Could not parse " + sourceFile.getAbsolutePath(), null);
	}

	@Override
	public RowReader createRowReader(IDataSourceSheet dataSourceSheet) throws UnsupportedDataFormatException, IOException {
		File sourceFile = dataSourceSheet.getSourceFile();
		String sheetName = dataSourceSheet.getSheetName();

		return new XlsxRowReader(sourceFile, sheetName);
	}
}
