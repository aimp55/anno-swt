/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.parser;

import java.io.File;

import org.genesys2.anno.gui.DataSourceSheet;

public class CsvDataSourceSheet extends DataSourceSheet {

	private String charset = "UTF-8";
	private char quoteChar = '"';
	private char delimiterChar = ',';
	private boolean unixEol = true;

	public CsvDataSourceSheet(File sourceFile) {
		super(sourceFile);
	}

	public String getCharset() {
		return this.charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
		System.err.println("Setting charset to " + charset);
		firePropertyChange("charset", null, this.charset);
	}

	public char getQuoteChar() {
		return this.quoteChar;
	}

	public void setQuoteChar(char quoteChar) {
		this.quoteChar = quoteChar;
		firePropertyChange("quoteChar", null, this.charset);
	}

	public char getDelimiterChar() {
		return this.delimiterChar;
	}

	public void setDelimiterChar(char delimiterChar) {
		this.delimiterChar = delimiterChar;
		firePropertyChange("delimiterChar", null, this.delimiterChar);
	}

	public String getEolSymbols() {
		return unixEol ? "\r" : "\r\n";
	}

	public boolean isUnixEol() {
		return unixEol;
	}

	public void setUnixEol(boolean unixEol) {
		this.unixEol = unixEol;
		firePropertyChange("unixEol", null, this.unixEol);
	}
}
