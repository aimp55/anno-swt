/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.anno.gui.DataSourceSheet;
import org.genesys2.anno.gui.IDataSourceSheet;
import org.genesys2.anno.gui.UnsupportedDataFormatException;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

public class CsvDataSourceParser extends BasicDataSourceParser {
	private static final Logger _log = Logger.getLogger(CsvDataSourceParser.class);

	private final String[] supportedExtensions = new String[] { ".csv", ".txt", ".tab" };

	@Override
	public String[] getSupportedExtensions() {
		return this.supportedExtensions;
	}

	@Override
	public Collection<? extends DataSourceSheet> findSheets(File sourceFile) throws UnsupportedDataFormatException {
		ArrayList<DataSourceSheet> sheets = new ArrayList<DataSourceSheet>();
		DataSourceSheet sheetDataSource = new CsvDataSourceSheet(sourceFile);
		sheetDataSource.setSheetName(sourceFile.getName());
		sheets.add(sheetDataSource);
		return sheets;
	}

	@Override
	public List<Object[]> loadRows(IDataSourceSheet dataSourceSheet, int maxRows, int startAt) throws UnsupportedDataFormatException, IOException {
		List<Object[]> rows = new ArrayList<Object[]>(maxRows);
		CsvListReader listReader = null;
		try {
			listReader = getListReader(dataSourceSheet);
			// rows.add(listReader.getHeader(true);

			// Skip rows
			for (int i = 0; i < startAt; i++) {
				_log.debug("Skipping row" + i);
				listReader.read();
			}

			for (int i = 0; i < maxRows; i++) {
				List<String> row = listReader.read();
				if (row == null)
					break;
				rows.add(row.toArray());
			}

			return rows;
			
		} catch (MalformedInputException e) {
			throw new IOException("Encoding error detected", e);
		} catch (IOException e) {
			throw e;
		} finally {
			if (listReader != null)
				listReader.close();
		}
	}

	private CsvListReader getListReader(IDataSourceSheet dataSourceSheet) throws FileNotFoundException {
		File sourceFile = dataSourceSheet.getSourceFile();

		String charsetName = "UTF-8";
		char quoteChar = '"';
		int delimiterChar = ',';
		String eolSymbols = "\r\n";

		if (dataSourceSheet instanceof CsvDataSourceSheet) {
			CsvDataSourceSheet csvSheet = (CsvDataSourceSheet) dataSourceSheet;
			charsetName = StringUtils.defaultIfBlank(csvSheet.getCharset(), charsetName);
			quoteChar = csvSheet.getQuoteChar();
			delimiterChar = csvSheet.getDelimiterChar();
			eolSymbols = csvSheet.getEolSymbols();
		}
		_log.info("Using charset " + charsetName);

		Reader reader = null;
		try {
			CharsetDecoder decoder = Charset.forName(charsetName).newDecoder();
	        decoder.onMalformedInput(CodingErrorAction.REPORT);

			reader = new InputStreamReader(new FileInputStream(sourceFile), decoder);
		} catch (FileNotFoundException e) {
			throw e;
		}

		CsvPreference preferences = new CsvPreference.Builder(quoteChar, delimiterChar, eolSymbols).build();
		CsvListReader listReader = new CsvListReader(reader, preferences);

		return listReader;
	}

	@Override
	public RowReader createRowReader(IDataSourceSheet dataSourceSheet) throws UnsupportedDataFormatException, IOException {
		try {
			CsvListReader listReader = getListReader(dataSourceSheet);

			CsvRowReader rowReader = new CsvRowReader(listReader);

			return rowReader;

		} catch (IOException e) {
			throw e;
		} finally {

		}
	}
}
